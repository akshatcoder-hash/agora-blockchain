# Contributing

## How do I... <a name="toc"></a>

* [Use This Guide](#introduction)?
* Ask or Say Something? 🤔🐛😱
  * [Request Support](#request-support)
  * [Report an Error or Bug](#report-an-error-or-bug)
  * [Request a Feature](#request-a-feature)
* Make Something? 🤓👩🏽‍💻📜🍳
  * [Project Setup](#project-setup)
  * [Contribute Documentation](#contribute-documentation)
  * [Contribute Code](#contribute-code)
* Manage Something ✅🙆🏼💃👔
  * [Provide Support on Issues](#provide-support-on-issues)
  * [Label Issues](#label-issues)
  * [Clean Up Issues and MRs](#clean-up-issues-and-prs)
  * [Review Merge Requests](#review-pull-requests)
  * [Merge Merge Requests](#merge-pull-requests)
  * [Tag a Release](#tag-a-release)
  * [Join the Project Team](#join-the-project-team)


## Introduction

Thank you so much for your interest in contributing!. All types of contributions are encouraged and valued. See the [table of contents](#toc) for different ways to help and details about how this project handles them!📝

Please make sure to read the relevant section before making your contribution! It will make it a lot easier for us maintainers to make the most of it and smooth out the experience for all involved. 💚

The Project Team looks forward to your contributions. 🙌🏾✨


## Request Support

If you have a question about this project, how to use it, or just need clarification about something:

* Open an Issue at https://gitlab.com/aossie/agora-blockchain/-/issues
* Provide as much context as you can about what you're running into.
* Provide project and platform versions (nodejs, npm, etc), depending on what seems relevant. If not, please be ready to provide that information if maintainers ask for it.

Once it's filed:

* The project team will [label the issue](#label-issues).
* Someone will try to have a response soon.
* If you or the maintainers don't respond to an issue for 30 days, the [issue will be closed](#clean-up-issues-and-prs). If you want to come back to it, reply (once, please), and we'll reopen the existing issue. Please avoid filing new issues as extensions of one you already made.


## Report an Error or Bug

If you run into an error or bug with the project:

* Open an Issue at https://gitlab.com/aossie/agora-blockchain/-/issues
* Include *reproduction steps* that someone else can follow to recreate the bug or error on their own.
* Provide project and platform versions (nodejs, npm, etc), depending on what seems relevant. If not, please be ready to provide that information if maintainers ask for it.

Once it's filed:

* The project team will [label the issue](#label-issues).
* A team member will try to reproduce the issue with your provided steps. If there are no repro steps or no obvious way to reproduce the issue, the team will ask you for those steps and mark the issue as `needs-repro`. Bugs with the `needs-repro` tag will not be addressed until they are reproduced.
* If the team is able to reproduce the issue, it will be marked `needs-fix`, as well as possibly other tags (such as `critical`), and the issue will be left to be [implemented by someone](#contribute-code).
* If you or the maintainers don't respond to an issue for 30 days, the [issue will be closed](#clean-up-issues-and-prs). If you want to come back to it, reply (once, please), and we'll reopen the existing issue. Please avoid filing new issues as extensions of one you already made.
* `critical` issues may be left open, depending on perceived immediacy and severity, even past the 30 day deadline.

## Request a Feature

If the project doesn't do something you need or want it to do:

* Open an Issue at https://gitlab.com/aossie/agora-blockchain/-/issues
* Provide as much context as you can about what you're running into.
* Please try and be clear about why existing features and alternatives would not work for you.

Once it's filed:

* The project team will [label the issue](#label-issues).
* The project team will evaluate the feature request, possibly asking you more questions to understand its purpose and any relevant requirements. If the issue is closed, the team will convey their reasoning and suggest an alternative path forward.
* If the feature request is accepted, it will be marked for implementation with `feature-accepted`, which can then be done by either by a core team member or by anyone in the community who wants to [contribute code](#contribute-code).

Note: The team is unlikely to be able to accept every single feature request that is filed. Please understand if they need to say no.

## Project Setup

So you wanna contribute some code! That's great! This project uses GitLab Merge Requests to manage contributions, so [read up on how to fork a GitLab project and file a MR](https://docs.gitlab.com/ee/user/project/merge_requests/) if you've never done it before.


If you want to go the usual route and run the project locally, though:

* [Install Node.js](https://nodejs.org/en/download/)
* [Fork the project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html)

**Clone the repository**

```bash
git clone https://gitlab.com/<your-username>/agora-blockchain/
```

**Switch to develop branch**

```bash
git checkout develop
```

### Setting up client side

Frontend code is present inside the `client` folder. Thus, the dependencies required for frontend are listed in the local `package.json` of `client` folder. You can install only the client-side dependencies using the following command.

**Install client-side dependencies**

```bash
npm run install-client --force
```

or

```bash
cd client
npm install
```

**Start frontend**

For development purpose we will run this command to start our client react app.

```bash
npm run client
```

or

```bash
cd client
npm start
```

**Build production for client**

> Only required while deploying frontend to some hosting services like AWS, Heroku, GCP etc.

This will create production build folder inside the `client` directory with the minified code of frontend. The production build will be served using `server.js` file inside the `client` folder.

```
npm run build
```

Deploying the repository to **Heroku** would automatically serve production build of the client side. This is because, **Heroku** uses `npm run build` for deploying our application. Once the `build` is complete, it uses the `npm start` command. In the `package.json` file of root directory, you can see `node client/server.js` command against `start` script. This will serve the minified files from `build` direcotry.

### Setting up Hardhat

Smart contracts or blockchain codes and necessary config files for developing, testing and deploying them are present inside `server` directory. Inside the `server` folder, there is `contracts` directory which stores **Agora Blockchain**'s business logic i.e. the smart contracts. `migrations` folder contains files for migrating smart contracts to the blockchain network. Go through these smart contracts and start contributing.

**About smart contracts**

We use Hardhat for editing, compiling, debugging and deploying your smart contracts and dApps, all of which work together to create a complete development environment. Compiled smart contracts or build files are then stored inside the `/artifacts/contracts` directory. You need to copy the required files to the frontend after compiling.

**Install server-side dependencies**

```bash
npm run install-server
```

or

```bash
cd server
npm install
```

> Now move inside `server` folder for the next steps.

**Compiling smart contracts**

If we have altered the code within our Solidity files (.sol) or made new ones or just want generate build files for the client, we need to run `npx hardhat compile` in the terminal. Compiled files are generated inside `/artifacts/contracts/File_name/`.

```bash
npx hardhat compile
```

**Create a .env file**

For deploying smart contracts on a blockchain network we need two things: A node connected to the network through which we will make RPC calls and an account with few coins, to cover the cost of deployment. [Infura](https://infura.io/) provides us the node for Ethereum network. So register there and put the Infura's API key inside of the `.env` file, like shown below.

```bash
MNEMONIC="<YOUR_SEED_GOES_HERE>"
KEY="<YOUR INFURA KEY GOES HERE>"
```

Also save your wallet's mnemonic phrase against the MNEMONIC in .env file, and don't forget to fund the account corresponding to that mnemonic. There are several faucets available to fund your account with test tokens.

> Never share or commit your `.env` file. It contains your credentials like `mnemonics` and `API` key. Therefore, it is advised to add `.env` to your `.gitignore` file.

**Deploying smart contracts**

You can deploy the smart contracts in the localhost network following these steps:

- Start a local node

```bash
npx hardhat node
```

- Open a new terminal and deploy the smart contract in the localhost network.


```bash
npx hardhat run --network localhost scripts/deploy.js
```

The above line will print the contract addresses which are deployed, mentioned in `deploy.js`.

Replace the contract addresses printed in the terminal with the contract addresses hard coded in the frontend files.

## Contribute Documentation

Documentation is a super important, critical part of this project. Docs are how we keep track of what we're doing, how, and why. It's how we stay on the same page about our policies. And it's how we tell others everything they need in order to be able to use this project -- or contribute to it. So thank you in advance.

Documentation contributions of any size are welcome! Feel free to file a MR even if you're just rewording a sentence to be more clear, or fixing a spelling mistake!

To contribute documentation:

* [Set up the project](#project-setup).
* Edit or add any relevant documentation.
* Make sure your changes are formatted correctly and consistently with the rest of the documentation.
* Re-read what you wrote, and run a spellchecker on it to make sure you didn't miss anything.
* Write clear, concise commit message(s) using [conventional-changelog format](https://docs.gitlab.com/ee/development/changelog.html). Documentation commits should use `docs(<component>): <message>`.
* Go to https://gitlab.com/aossie/agora-blockchain/-/merge_requests and open a new merge request with your changes.
* If your MR is connected to an open issue, add a line in your MR's description that says `Fixes: #123`, where `#123` is the number of the issue you're fixing.

Once you've filed the MR:

* One or more maintainers will use GitLab's review feature to review your MR.
* If the maintainer asks for any changes, edit your changes, push, and ask for another review.
* If the maintainer decides to pass on your MR, they will thank you for the contribution and explain why they won't be accepting the changes. That's ok! We still really appreciate you taking the time to do it, and we don't take that lightly. 💚
* If your MR gets accepted, it will be marked as such, and merged into the `latest` branch soon after. Your contribution will be distributed to the masses next time the maintainers [tag a release](#tag-a-release)

## Contribute Code

We like code commits a lot! They're super handy, and they keep the project going and doing the work it needs to do to be useful to others.

Code contributions of just about any size are acceptable!

The main difference between code contributions and documentation contributions is that contributing code requires inclusion of relevant tests for the code being added or changed. Contributions without accompanying tests will be held off until a test is added, unless the maintainers consider the specific tests to be either impossible, or way too much of a burden for such a contribution.

To contribute code:

* [Set up the project](#project-setup).
* Make any necessary changes to the source code.
* Include any [additional documentation](#contribute-documentation) the changes might need.
* Write tests that verify that your contribution works as expected.
* Write clear, concise commit message(s) using [conventional-changelog format](https://docs.gitlab.com/ee/development/changelog.html).
* Dependency updates, additions, or removals must be in individual commits, and the message must use the format: `<prefix>(deps): PKG@VERSION`, where `<prefix>` is any of the usual `conventional-changelog` prefixes, at your discretion.
* Go to https://gitlab.com/aossie/agora-blockchain/-/merge_requests and open a new merge request with your changes.
* If your MR is connected to an open issue, add a line in your MR's description that says `Fixes: #123`, where `#123` is the number of the issue you're fixing.

Once you've filed the MR:

* Barring special circumstances, maintainers will not review MRs until all checks pass (Travis, AppVeyor, etc).
* One or more maintainers will use GitLab's review feature to review your MR.
* If the maintainer asks for any changes, edit your changes, push, and ask for another review. Additional tags (such as `needs-tests`) will be added depending on the review.
* If the maintainer decides to pass on your MR, they will thank you for the contribution and explain why they won't be accepting the changes. That's ok! We still really appreciate you taking the time to do it, and we don't take that lightly. 💚
* If your MR gets accepted, it will be marked as such, and merged into the `latest` branch soon after. Your contribution will be distributed to the masses next time the maintainers [tag a release](#tag-a-release)

## Provide Support on Issues

[Needs Collaborator](#join-the-project-team): Issue Tracker

Helping out other users with their questions is a really awesome way of contributing to any community. It's not uncommon for most of the issues on an open source projects being support-related questions by users trying to understand something they ran into, or find their way around a known bug.

Sometimes, the `support` label will be added to things that turn out to actually be other things, like bugs or feature requests. In that case, suss out the details with the person who filed the original issue, add a comment explaining what the bug is, and change the label from `support` to `bug` or `feature`. If you can't do this yourself, @mention a maintainer so they can do it.

In order to help other folks out with their questions:

* Go to the issue tracker and [filter open issues by the `support` label](https://gitlab.com/aossie/agora-blockchain/-/issues?q=is%3Aopen+is%3Aissue+label%3Asupport).
* Read through the list until you find something that you're familiar enough with to give an answer to.
* Respond to the issue with whatever details are needed to clarify the question, or get more details about what's going on.
* Once the discussion wraps up and things are clarified, either close the issue, or ask the original issue filer (or a maintainer) to close it for you.

Some notes on picking up support issues:

* Avoid responding to issues you don't know you can answer accurately.
* As much as possible, try to refer to past issues with accepted answers. Link to them from your replies with the `#123` format.
* Be kind and patient with users -- often, folks who have run into confusing things might be upset or impatient. This is ok. Try to understand where they're coming from, and if you're too uncomfortable with the tone, feel free to stay away or withdraw from the issue. 

## Label Issues

[Needs Collaborator](#join-the-project-team): Issue Tracker

One of the most important tasks in handling issues is labeling them usefully and accurately. All other tasks involving issues ultimately rely on the issue being classified in such a way that relevant parties looking to do their own tasks can find them quickly and easily.

In order to label issues, [open up the list of unlabeled issues](https://gitlab.com/aossie/agora-blockchain/-/issues?q=is%3Aopen+is%3Aissue+no%3Alabel) and, **from newest to oldest**, read through each one and apply issue labels according to the table below. If you're unsure about what label to apply, skip the issue and try the next one: don't feel obligated to label each and every issue yourself!

Label | Apply When | Notes
--- | --- | ---
`bug` | Cases where the code (or documentation) is behaving in a way it wasn't intended to. | If something is happening that surprises the *user* but does not go against the way the code is designed, it should use the `enhancement` label.
`critical` | Added to `bug` issues if the problem described makes the code completely unusable in a common situation. |
`documentation` | Added to issues or Merge Requests that affect any of the documentation for the project. | Can be combined with other labels, such as `bug` or `enhancement`.
`duplicate` | Added to issues or MRs that refer to the exact same issue as another one that's been previously labeled. | Duplicate issues should be marked and closed right away, with a message referencing the issue it's a duplicate of (with `#123`)
`enhancement` | Added to [feature requests](#request-a-feature), MRs, or documentation issues that are purely additive: the code or docs currently work as expected, but a change is being requested or suggested. |
`help wanted` | Applied by [Committers](#join-the-project-team) to issues and MRs that they would like to get outside help for. Generally, this means it's lower priority for the maintainer team to itself implement, but that the community is encouraged to pick up if they so desire | Never applied on first-pass labeling.
`in-progress` | Applied by [Committers](#join-the-project-team) to MRs that are pending some work before they're ready for review. | The original MR submitter should @mention the team member that applied the label once the MR is complete.
`performance` | This issue or MR is directly related to improving performance. |
`refactor` | Added to issues or MRs that deal with cleaning up or modifying the project for the betterment of it. |
`starter` | Applied by [Committers](#join-the-project-team) to issues that they consider good introductions to the project for people who have not contributed before. These are not necessarily "easy", but rather focused around how much context is necessary in order to understand what needs to be done for this project in particular. | Existing project members are expected to stay away from these unless they increase in priority.
`support` | This issue is either asking a question about how to use the project, clarifying the reason for unexpected behavior, or possibly reporting a `bug` but does not have enough detail yet to determine whether it would count as such. | The label should be switched to `bug` if reliable reproduction steps are provided. Issues primarily with unintended configurations of a user's environment are not considered bugs, even if they cause crashes.
`tests` | This issue or MR either requests or adds primarily tests to the project. | If a MR is pending tests, that will be handled through the [MR review process](#review-pull-requests)
`wontfix` | Labelers may apply this label to issues that clearly have nothing at all to do with the project or are otherwise entirely outside of its scope/sphere of influence. [Committers](#join-the-project-team) may apply this label and close an issue or MR if they decide to pass on an otherwise relevant issue. | The issue or MR should be closed as soon as the label is applied, and a clear explanation provided of why the label was used. Contributors are free to contest the labeling, but the decision ultimately falls on committers as to whether to accept something or not.

## Clean Up Issues and MRs

[Needs Collaborator](#join-the-project-team): Issue Tracker

Issues and MRs can go stale after a while. Maybe they're abandoned. Maybe the team will just plain not have time to address them any time soon.

In these cases, they should be closed until they're brought up again or the interaction starts over.

To clean up issues and MRs:

* Search the issue tracker for issues or MRs, and add the term `updated:<=YYYY-MM-DD`, where the date is 30 days before today.
* Go through each issue *from oldest to newest*, and close them if **all of the following are true**:
  * not opened by a maintainer
  * not marked as `critical`
  * not marked as `starter` or `help wanted` (these might stick around for a while, in general, as they're intended to be available)
  * no explicit messages in the comments asking for it to be left open
  * does not belong to a milestone
* Leave a message when closing saying "Cleaning up stale issue. Please reopen or ping us if and when you're ready to resume this.


## Review Merge Requests

[Needs Collaborator](#join-the-project-team): Issue Tracker

While anyone can comment on a MR, add feedback, etc, MRs are only *approved* by team members with Issue Tracker or higher permissions.

MR reviews use [GitLab's own review feature](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/), which manages comments, approval, and review iteration.

Some notes:

* You may ask for minor changes ("nitpicks"), but consider whether they are really blockers to merging: try to err on the side of "approve, with comments".
* *ALL Merge Requests* should be covered by a test: either by a previously-failing test, an existing test that covers the entire functionality of the submitted code, or new tests to verify any new/changed behavior. All tests must also pass and follow established conventions. Test coverage should not drop, unless the specific case is considered reasonable by maintainers.
* Please make sure you're familiar with the code or documentation being updated, unless it's a minor change (spellchecking, minor formatting, etc). You may @mention another project member who you think is better suited for the review, but still provide a non-approving review of your own.
* Be extra kind: people who submit code/doc contributions are putting themselves in a pretty vulnerable position, and have put time and care into what they've done (even if that's not obvious to you!) -- always respond with respect, be understanding, but don't feel like you need to sacrifice your standards for their sake, either. Just don't be a jerk about it?

## Approve Merge Requests

[Needs Collaborator](#join-the-project-team): Committer

TBD - need to hash out a bit more of this process.

## Tag A Release

[Needs Collaborator](#join-the-project-team): Committer

TBD - need to hash out a bit more of this process. The most important bit here is probably that all tests must pass, and tags must use [semver](https://semver.org).

## Join the Project Team

### Ways to Join

There are many ways to contribute! Most of them don't require any official status unless otherwise noted. That said, there's a couple of positions that grant special repository abilities, and this section describes how they're granted and what they do.

All of the below positions are granted based on the project team's needs, as well as their consensus opinion about whether they would like to work with the person and think that they would fit well into that position. The process is relatively informal, and it's likely that people who express interest in participating can just be granted the permissions they'd like.

You can spot a collaborator on the repo by looking for the `[Collaborator]` or `[Owner]` tags next to their names.

Permission | Description
--- | ---
Issue Tracker | Granted to contributors who express a strong interest in spending time on the project's issue tracker. These tasks are mainly [labeling issues](#label-issues), [cleaning up old ones](#clean-up-issues-and-prs), and [reviewing Merge Requests](#review-pull-requests), as well as all the usual things non-team-member contributors can do. Issue handlers should not merge Merge Requests, tag releases, or directly commit code themselves: that should still be done through the usual merge request process. Becoming an Issue Handler means the project team trusts you to understand enough of the team's process and context to implement it on the issue tracker.
Committer | Granted to contributors who want to handle the actual merge request merges, tagging new versions, etc. Committers should have a good level of familiarity with the codebase, and enough context to understand the implications of various changes, as well as a good sense of the will and expectations of the project team.
Admin/Owner | Granted to people ultimately responsible for the project, its community, etc.
